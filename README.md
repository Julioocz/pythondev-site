# Pydev Site

[![build status](https://gitlab.com/PythonDevCommunity/pythondev-site/badges/master/build.svg)](https://gitlab.com/PythonDevCommunity/pythondev-site/commits/master)

A website built for the pythondev community of slack.

Join us: http://pythondevelopers.herokuapp.com/

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisities

You will need to have installed in you pc Python 3.5+, git and virtualenv for local development.

Go to https://www.python.org/downloads/ to install python and then you can install virtualenv with pip

If you are using Linux you could install `autovenv` too, that way when you cd to this project the virtualenv
env will be activated automatically 

```
pip install virtualenv

#Linux
pip install autoenv
```

### Installing

The first you need to do is clone this project to a local directory
Open a terminal or a windows console in the directory where you
want to save the project and type:

```
Git clone https://gitlab.com/PythonDevCommunity/pythondev-site.py
cd pythondev-site
```

Now you have a clone of this awesome project in your pc and the current
directory of your terminal is the project.

Now you need to create a python virtualenv where you will install all the requirements
of the site

```
virtualenv venv
```
To activate the virtualenv you can do the following:
```
# Windows
venv\Scripts\activate

# Linux
source venv/bin/activate
```
Now you can start all the requeriments in the virtualenv
to get Started
```
pip install -r requirements.txt
```

Now you are good to go!. You can start the site in your
local pc typing 
```
python run.py
```

Go to [http://localhost:5000/] to see the results ;)


## Running the tests

We have no tests at the moment :(

### Break down into end to end tests

No tests, we like to live to the limit


## Deployment

This app is ready to be deployed in heroku!.. click 
[Here](https://devcenter.heroku.com/articles/getting-started-with-python#introduction)
for more info!.

## Built With

* Love
* An awesome community
* Skift ~~money~~ templates
* Flask!

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Gitlab](https://gitlab.com/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/PythonDevCommunity/pythondev-site/tags). 


## Authors

* **Pythondev Slack Community** - [Gitlab](https://gitlab.com/PythonDevCommunity)

See the list of [contributors of the community](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the Apache License - see the [LICENSE.md](LICENSE) file for details

## Acknowledgments
* To the Awesome Pytondev Community in slack