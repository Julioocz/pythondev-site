import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
# Configuration for windows peasants
if os.environ.get('APP_SETTINGS') is not None:
    app.config.from_object(os.environ['APP_SETTINGS'])
else:
    app.config.from_object('config.DevelopmentConfig')

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
